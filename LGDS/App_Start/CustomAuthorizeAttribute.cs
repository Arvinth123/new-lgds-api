﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace LGDS
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);
            //Logger.CreateErrorLog(actionContext.Request.ToString());
        }
    }

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class CustomActionFilterAttribute : ActionFilterAttribute
    {
        private Stopwatch stopwatch = new Stopwatch();
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);

            if (actionContext != null)
            {
                stopwatch.Start();
                string message = string.Format("Process started by {2} : {0}.{1}", actionContext.ActionDescriptor.ControllerDescriptor.ControllerName
                                                                                 , actionContext.ActionDescriptor.ActionName
                                                                                 , actionContext.RequestContext.Principal.Identity.Name);
                //Logger.CreateInformationLog(message);
            }
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);

            if (actionExecutedContext != null)
            {
                stopwatch.Stop();
                string message = string.Format("Process completed by {3}: {0}.{1}. Elapsed Time : {2}", actionExecutedContext.ActionContext.ActionDescriptor.ControllerDescriptor.ControllerName
                                                                                                      , actionExecutedContext.ActionContext.ActionDescriptor.ActionName
                                                                                                      , stopwatch.Elapsed
                                                                                                      , actionExecutedContext.ActionContext.RequestContext.Principal.Identity.Name);
               // Logger.CreateInformationLog(message);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class CustomActionExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            base.OnException(context);

            if (context != null)
            {
              //  Logger.CreateErrorLog(context.Exception.Message);
              //  Logger.CreateErrorLog(context.Request.ToString());
            }
        }
    }
}