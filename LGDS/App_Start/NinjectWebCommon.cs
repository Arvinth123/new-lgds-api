﻿
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(LGDS.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(LGDS.App_Start.NinjectWebCommon), "Stop")]
namespace LGDS.App_Start
{
    using LGDS;
using LGDS.Connectors;
using LGDS.Interfaces;
    using LGDS.Mapper;
    using LGDS.Mappers;
    using LGDS.Models;
    using LGDSDTO.DTO;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Moq.DTO.DTO;
    using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Ninject.Syntax;
using Ninject.Web.Common;
using Ninject.Web.WebApi;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dependencies;


  public static class NinjectWebCommon
  {
    private static readonly Bootstrapper bootstrapper = new Bootstrapper();

    public static void Start()
    {
      DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
      DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

    public static void Stop()
    {
            bootstrapper.ShutDown();
        }

    private static IKernel CreateKernel()
    {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                RegisterServices(kernel);
                return kernel;
            }
            catch
      {
                kernel.Dispose();
                throw;
      }
    }

    private static void RegisterServices(IKernel kernel)
    {
      kernel.Bind<IConnector<TransactionalDataDTO>>().To<POHDetailsConnector>();
      kernel.Bind<IConnector<CoachMasterDTO>>().To<CoachMasterConnecters>();
      kernel.Bind<ICoachDetails<CoachSearchDTO>>().To<CoachSearchConnector>();
      kernel.Bind<IWorkshop<WorkshopDTO>>().To<WorkshopConnector>();
      kernel.Bind<ICoachId<CoachIdDTO>>().To<CoachIdConnector>();
      kernel.Bind<IPOHDetails<POHsearchDTO>>().To<POHsearchConnector>();
      kernel.Bind<IOutTurn<OutTurnDTO>>().To<OutTurnConnector>();
      kernel.Bind<Icoachidshop<coachidshopDTO>>().To<coachidshopConnector>();
      kernel.Bind<IInTurn<InTurnDTO>>().To<InTurnConnector>();
      kernel.Bind<ICoachDetails<CoachidholdingDTO>>().To<CoachidholdingConnector>();
      kernel.Bind<IInTurn<CoachidinturnDTO>>().To<CoachidinturnConnector>();
      kernel.Bind<IOutTurn<CoachidoutturnDTO>>().To<CoachidoutturnConnector>();
      kernel.Bind<ICodefilter<codefilterDTO>>().To<codefilterConnector>();
      kernel.Bind<ILogin<LoginDTO>>().To<LoginConnector>();
      kernel.Bind<IUnit<UnitDTO>>().To<UnitConnector>();
      kernel.Bind<IWorkdays<WorkdaysDTO>>().To<WorkdaysConnector>();
      kernel.Bind<IMonths<MonthDTO>>().To<MonthsConnector>();
      kernel.Bind<IYear<YearDTO>>().To<YearConnector>();
      kernel.Bind<ITotal<TotalDTO>>().To<TotalConnector>();
      kernel.Bind<IRating<RatingDTO>>().To<RatingConnector>();
      kernel.Bind<IReportPDF<ReportPDFDTO>>().To<ReportPDFConnector>();
      kernel.Bind<IMapper<tbl_TransactionalData, TransactionalDataDTO>>().To<TransactionalDataMapper>();
      kernel.Bind<IMapper<tbl_CoachMaster, CoachMasterDTO>>().To<CoachMasterMapper>();
      kernel.Bind<CoachMaster_V_2018Entities>().To<CoachMaster_V_2018Entities>();
    }
  }
}
