﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Controllers.WorkshopController
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS.Interfaces;
using LGDSDTO;
using LGDSDTO.DTO;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LGDS.Controllers
{
  [CustomActionException]
  [AllowAnonymous]
  public class WorkshopController : ApiController
  {
    private IWorkshop<WorkshopDTO> searchDetails;

    public WorkshopController(IWorkshop<WorkshopDTO> _searchDetails)
    {
      this.searchDetails = _searchDetails;
    }

    [HttpGet]
    public HttpResponseMessage GetDirectoryListing()
    {
      IEnumerable<WorkshopDTO> workshopDtos = this.searchDetails.Getall();
      if (workshopDtos == null)
        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
        {
          Content = (HttpContent) new StringContent(string.Format("No Data found")),
          ReasonPhrase = "Data  Not Found"
        });
      return this.Request.CreateResponse<IEnumerable<WorkshopDTO>>(HttpStatusCode.OK, workshopDtos);
    }
  }
}
