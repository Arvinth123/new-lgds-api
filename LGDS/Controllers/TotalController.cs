﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Controllers.TotalController
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO;
using LGDSDTO.DTO;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LGDS.Controllers
{
  [CustomActionException]
  [AllowAnonymous]
  public class TotalController : ApiController
  {
    private ITotal<TotalDTO> searchDetails;

    public TotalController(ITotal<TotalDTO> _searchDetails)
    {
      this.searchDetails = _searchDetails;
    }

    [HttpGet]
    public HttpResponseMessage GetDirectoryListing([FromUri] CoachPositionModel model)
    {
      return this.Request.CreateResponse<IEnumerable<TotalDTO>>(HttpStatusCode.OK, this.searchDetails.Getbyid(model));
    }
  }
}
