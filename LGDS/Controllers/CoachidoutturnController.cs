﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Controllers.CoachidoutturnController
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO;
using LGDSDTO.DTO;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LGDS.Controllers
{
  [CustomActionException]
  [AllowAnonymous]
  public class CoachidoutturnController : ApiController
  {
    private IOutTurn<CoachidoutturnDTO> searchDetails;

    public CoachidoutturnController(IOutTurn<CoachidoutturnDTO> _searchDetails)
    {
      this.searchDetails = _searchDetails;
    }

    [HttpGet]
    public HttpResponseMessage GetDirectoryListing([FromUri] OutTurnModel model)
    {
      return this.Request.CreateResponse<IEnumerable<CoachidoutturnDTO>>(HttpStatusCode.OK, this.searchDetails.Getbyid(model));
    }
  }
}
