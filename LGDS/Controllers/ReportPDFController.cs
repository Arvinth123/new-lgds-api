﻿using LGDS.Interfaces;
using LGDS.Models;
using Moq.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LGDS.Controllers
{
    [CustomActionException]
    [AllowAnonymous]
    public class ReportPDFController : ApiController
    {
        private IReportPDF<ReportPDFDTO> ReportPDF;

        public ReportPDFController(IReportPDF<ReportPDFDTO> _ReportPDF)
        {
            this.ReportPDF = _ReportPDF;
        }

        [HttpGet]
        public HttpResponseMessage GetReport([FromUri] ReportPDFModel model)
        {
            IList<string> all = this.ReportPDF.Getbyid(model);
            if (all == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = (HttpContent)new StringContent(string.Format("No Data found")),
                    ReasonPhrase = "Data  Not Found"
                });
            return this.Request.CreateResponse<IList<string>>(HttpStatusCode.OK, all);
        }

        [Route("api/ReportPDF/GetReportPDF")]
        [HttpGet]
        public HttpResponseMessage GetReportPDF([FromUri] DateTime date)
        {
            IEnumerable<ReportPDFDTO> all = this.ReportPDF.Getbydate(date);
            if (all == null)
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = (HttpContent)new StringContent(string.Format("No Data found")),
                    ReasonPhrase = "Data  Not Found"
                });
            return this.Request.CreateResponse<IEnumerable<ReportPDFDTO>>(HttpStatusCode.OK, all);
        }
    }
}
