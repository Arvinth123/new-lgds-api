﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Controllers.CoachMasterController
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS.Interfaces;
using LGDSDTO;
using LGDSDTO.DTO;
using Moq.DTO.DTO;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LGDS.Controllers
{
  [CustomActionException]
  [AllowAnonymous]
  public class CoachMasterController : ApiController
  {
    private IConnector<CoachMasterDTO> CoachMaster;
    private IConnector<TransactionalDataDTO> POHDetails;
    private ICoachDetails<CoachMasterDTO> CoachSearch;

    public CoachMasterController(IConnector<CoachMasterDTO> _CoachMaster, IConnector<TransactionalDataDTO> _POHDetails, ICoachDetails<CoachMasterDTO> _CoachSearch)
    {
      this.CoachMaster = _CoachMaster;
      this.POHDetails = _POHDetails;
      this.CoachSearch = _CoachSearch;
    }

    [HttpGet]
    public HttpResponseMessage GetCoachMaster()
    {
      IEnumerable<CoachMasterDTO> all = this.CoachMaster.GetAll();
      if (all == null)
        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
        {
          Content = (HttpContent) new StringContent(string.Format("No Data found")),
          ReasonPhrase = "Data  Not Found"
        });
      return this.Request.CreateResponse<IEnumerable<CoachMasterDTO>>(HttpStatusCode.OK, all);
    }

    [HttpGet]
    public HttpResponseMessage GetCoachMaster(string id)
    {
      IEnumerable<CoachMasterDTO> byId = this.CoachMaster.GetById(id);
      if (byId == null)
        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
        {
          Content = (HttpContent) new StringContent(string.Format("No Data found")),
          ReasonPhrase = "Data  Not Found"
        });
      return this.Request.CreateResponse<IEnumerable<CoachMasterDTO>>(HttpStatusCode.OK, byId);
    }
  }
}
