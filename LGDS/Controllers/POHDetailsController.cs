﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Controllers.POHDetailsController
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS.Interfaces;
using LGDSDTO;
using LGDSDTO.DTO;
using Moq.DTO.DTO;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LGDS.Controllers
{
  [CustomActionException]
  [AllowAnonymous]
  public class POHDetailsController : ApiController
  {
    private IConnector<TransactionalDataDTO> POHDetails;

    public POHDetailsController(IConnector<TransactionalDataDTO> _POHDetails)
    {
      this.POHDetails = _POHDetails;
    }

    [HttpGet]
    public HttpResponseMessage Gettbl_POHDetails()
    {
      IEnumerable<TransactionalDataDTO> all = this.POHDetails.GetAll();
      if (all == null)
        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
        {
          Content = (HttpContent) new StringContent(string.Format("No Data found")),
          ReasonPhrase = "Data  Not Found"
        });
      return this.Request.CreateResponse<IEnumerable<TransactionalDataDTO>>(HttpStatusCode.OK, all);
    }

    [HttpGet]
    public HttpResponseMessage Gettbl_POHDetails(string id)
    {
      IEnumerable<TransactionalDataDTO> byId = this.POHDetails.GetById(id);
      if (byId == null)
        throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
        {
          Content = (HttpContent) new StringContent(string.Format("No Data found")),
          ReasonPhrase = "Data  Not Found"
        });
      return this.Request.CreateResponse<IEnumerable<TransactionalDataDTO>>(HttpStatusCode.OK, byId);
    }
  }
}
