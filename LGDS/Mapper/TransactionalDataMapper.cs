﻿using LGDS.Interfaces;
using LGDS.Models;
using Moq.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LGDS.Mapper
{
    public class TransactionalDataMapper : IMapper<tbl_TransactionalData, TransactionalDataDTO>
    {
        public TransactionalDataDTO Map(tbl_TransactionalData tbl_POHDetails, TransactionalDataDTO tbl_POHDetailsDTO)
        {
            if (tbl_POHDetails == null)
                return (TransactionalDataDTO)null;
            tbl_POHDetailsDTO = new TransactionalDataDTO()
            {
                CoachNo = tbl_POHDetails.CoachNo,
                LastPOH = tbl_POHDetails.LastPOH,
                PreviousReturnDate = tbl_POHDetails.PreviousReturnDate,
                YardIN_Date = tbl_POHDetails.YardIN_Date,
                IN_Date = tbl_POHDetails.IN_Date,
                NC_Date = tbl_POHDetails.NC_Date,
                Date_OUT = tbl_POHDetails.Date_OUT,
                POH_Shop = tbl_POHDetails.POH_Shop,
                RepairType = tbl_POHDetails.RepairType,
                WorkOrder_No = tbl_POHDetails.WorkOrder_No,
                Rake_Formation = tbl_POHDetails.Rake_Formation,
                Return_Date = tbl_POHDetails.Return_Date,
                Ext_Return_Date = tbl_POHDetails.Ext_Return_Date,
                IOH_Station = tbl_POHDetails.IOH_Station,
                IOH_Date = tbl_POHDetails.IOH_Date,
                CORR_HRS = tbl_POHDetails.CORR_HRS,
                Corr_Decleration = tbl_POHDetails.Corr_Decleration,
                CYCLETIME_CATEGORY = tbl_POHDetails.CYCLETIME_CATEGORY,
                POH_Status = tbl_POHDetails.POH_Status
            };
            return tbl_POHDetailsDTO;
        }
    }
}