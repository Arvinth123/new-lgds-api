﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Mappers.CoachMasterMapper
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;

namespace LGDS.Mappers
{
  public class CoachMasterMapper : IMapper<tbl_CoachMaster, CoachMasterDTO>
  {
    public CoachMasterDTO Map(tbl_CoachMaster tbl_CoachMaster, CoachMasterDTO tbl_CoachMasterDTO)
    {
      if (tbl_CoachMaster == null)
        return (CoachMasterDTO) null;
      tbl_CoachMasterDTO = new CoachMasterDTO()
      {
        CoachNo = tbl_CoachMaster.CoachNo,
        Code = tbl_CoachMaster.Code,
        Type = tbl_CoachMaster.Type,
        Built = tbl_CoachMaster.Built,
        Railway = tbl_CoachMaster.Railway,
        VehicleType = tbl_CoachMaster.VehicleType,
        Category = tbl_CoachMaster.Category,
        AC_Flag = tbl_CoachMaster.AC_Flag,
        CoachID = tbl_CoachMaster.CoachID,
        BaseDepot = tbl_CoachMaster.BaseDepot,
        OwningDivision = tbl_CoachMaster.OwningDivision,
        Base = tbl_CoachMaster.Base,
        Workshop = tbl_CoachMaster.Workshop,
        BuiltDate = tbl_CoachMaster.BuiltDate,
        InductionDate = tbl_CoachMaster.InductionDate,
        CouplingType = tbl_CoachMaster.CouplingType,
        Coupling_Make = tbl_CoachMaster.Coupling_Make,
        BrakeSystem = tbl_CoachMaster.BrakeSystem,
        ToiletSystem = tbl_CoachMaster.ToiletSystem,
        Toilet_Make = tbl_CoachMaster.Toilet_Make,
        Periodicity = tbl_CoachMaster.Periodicity,
        Status = tbl_CoachMaster.Status,
        Transfered_From = tbl_CoachMaster.Transfered_From,
        Transfered_To = tbl_CoachMaster.Transfered_To,
        Transfered_From_Date = tbl_CoachMaster.Transfered_From_Date,
        Transfered_To_Date = tbl_CoachMaster.Transfered_To_Date,
        Condemnation_No = tbl_CoachMaster.Condemnation_No,
        Condemnation_Date = tbl_CoachMaster.Condemnation_Date,
        MLR_Shop = tbl_CoachMaster.MLR_Shop,
        MLR_Date = tbl_CoachMaster.MLR_Date,
        RF_Shop = tbl_CoachMaster.RF_Shop,
        RF_Date = tbl_CoachMaster.RF_Date,
        US_RM = tbl_CoachMaster.US_RM,
        UpdationType = tbl_CoachMaster.UpdationType,
        NewCoachLrNo = tbl_CoachMaster.NewCoachLrNo,
        NewCoachLrDate = tbl_CoachMaster.NewCoachLrDate,
        OldCoachNo = tbl_CoachMaster.OldCoachNo,
        No_of_Tanks = tbl_CoachMaster.No_of_Tanks,
        Remarks = tbl_CoachMaster.Remarks,
        NextPOHDueOn = tbl_CoachMaster.NextPOHDueOn
      };
      return tbl_CoachMasterDTO;
    }
  }
}
