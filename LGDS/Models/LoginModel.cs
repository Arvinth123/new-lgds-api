using System;

namespace LGDS.Models
{
	public class LoginModel
	{
		public string username
		{
			get;
			set;
		}

		public string password
		{
			get;
			set;
		}

		public string unit
		{
			get;
			set;
		}
	}
}
