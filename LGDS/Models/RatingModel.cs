using System;

namespace LGDS.Models
{
	public class RatingModel
	{
		public string username
		{
			get;
			set;
		}

		public DateTime? date
		{
			get;
			set;
		}

		public int? rating
		{
			get;
			set;
		}

		public string review
		{
			get;
			set;
		}
	}
}
