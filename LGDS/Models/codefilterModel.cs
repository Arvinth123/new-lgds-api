using System;

namespace LGDS.Models
{
	public class codefilterModel
	{
		public string Workshopid
		{
			get;
			set;
		}

		public string code
		{
			get;
			set;
		}

		public string datein
		{
			get;
			set;
		}

		public string mfgtype
		{
			get;
			set;
		}

		public string division
		{
			get;
			set;
		}
	}
}
