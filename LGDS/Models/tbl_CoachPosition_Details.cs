//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LGDS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_CoachPosition_Details
    {
        public string WorkingDays { get; set; }
        public string MonthTarget_Outturn { get; set; }
        public string MonthTarget_AC { get; set; }
        public string MonthTarget_NonAC { get; set; }
        public string MonthTarget_SPV { get; set; }
        public string MonthTarget_LHB { get; set; }
        public string TillDateTarget_Outturn { get; set; }
        public string TillDateTarget_AC { get; set; }
        public string TillDateTarget_NonAC { get; set; }
        public string TillDateTarget_SPV { get; set; }
        public string TillDateTarget_LHB { get; set; }
        public string ActualOutturn_Outturn { get; set; }
        public string ActualOutturn_AC { get; set; }
        public string ActualOutturn_NonAC { get; set; }
        public string ActualOutturn_SPV { get; set; }
        public string ActualOutturn_LHB { get; set; }
        public string Year_Outturn_Target { get; set; }
        public string Year_Outturn_Actual { get; set; }
        public string Year_AC_Target { get; set; }
        public string Year_AC_Actual { get; set; }
        public string Year_NAC_Target { get; set; }
        public string Year_NAC_Actual { get; set; }
        public string Year_SPV_Target { get; set; }
        public string Year_SPV_Actual { get; set; }
        public string Year_LHB_Target { get; set; }
        public string Year_LHB_Actual { get; set; }
        public string FYear { get; set; }
        public string Target { get; set; }
        public string Actual { get; set; }
        public string Status { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
}
