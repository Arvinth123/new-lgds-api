﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LGDS.Models
{
    public class ReportPDFModel
    {
        public string Day
        {
            get;
            set;
        }

        public string Month
        {
            get;
            set;
        }

        public string Year
        {
            get;
            set;
        }
    }
}