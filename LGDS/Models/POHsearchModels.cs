using System;

namespace LGDS.Models
{
	public class POHsearchModels
	{
		public string CoachNo
		{
			get;
			set;
		}

		public string own_rlw
		{
			get;
			set;
		}

		public string Trp_Code
		{
			get;
			set;
		}

		public string Mfg_Type
		{
			get;
			set;
		}

		public string LPOHDate
		{
			get;
			set;
		}

		public string PRetDate
		{
			get;
			set;
		}
	}
}
