using System;

namespace LGDS.Models
{
	public class CoachPositionModel
	{
		public string Month
		{
			get;
			set;
		}

		public string Year
		{
			get;
			set;
		}
	}
}
