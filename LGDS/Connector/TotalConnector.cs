﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.TotalConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class TotalConnector : ITotal<TotalDTO>
  {
    private CoachMaster_V_2018Entities context;

    public TotalConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

        public IEnumerable<TotalDTO> Getbyid(CoachPositionModel model)
        {
            IEnumerable<uspctotal_Result> total = GetTotal(model);
            if (total.Any())
            {
                IEnumerable<TotalDTO> p = total.Select(Total => new TotalDTO()
                {
                    name = Total.name,
                    year = Total.year,
                    target1 = Total.target1,
                    actual1 = Total.actual1,
                    status1 = Total.status1
                });
            return p;
           }
      return null;
    }

    public IEnumerable<uspctotal_Result> GetTotal(CoachPositionModel model)
    {
      string month = string.Empty;
      string year = string.Empty;
      if (!string.IsNullOrEmpty(model.Month))
        month = model.Month;
      if (!string.IsNullOrEmpty(model.Year))
        year = model.Year;
            var searchResult = context.uspctotal(month, year).ToList();
            return searchResult;
    }
  }
}
