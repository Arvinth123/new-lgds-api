﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.CoachSearchConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LGDS.Connectors
{
  public class CoachSearchConnector : ICoachDetails<CoachSearchDTO>
  {
    private CoachMaster_V_2018Entities context;

    public CoachSearchConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

        public IEnumerable<CoachSearchDTO> Getbyid(CoachSearchModel model)
        {
            IEnumerable<uspcCoachSearch_Result> coachSearch = GetCoachSearch(model);
            if (coachSearch.Any())
            { 
                IEnumerable<CoachSearchDTO> p = coachSearch.Select(CoachSearch => new CoachSearchDTO()
                {
                    CoachNo = CoachSearch.CoachNo,
                    SerailNo = CoachSearch.serial_number,
                    Code = CoachSearch.Code,
                    LPOHDate = string.Format("{0:dd-MM-yyyy}", CoachSearch.LastPOH),
                    RetDate = string.Format("{0:dd-MM-yyyy}", CoachSearch.PreviousReturnDate),
                    DateIN = CoachSearch.IN_Date.ToString("dd-MM-yyyy"),
                    Mfg_Type = CoachSearch.Type,
                    Owning_Railway = CoachSearch.Railway,
                    Division = CoachSearch.OwningDivision,
                    Built = CoachSearch.Built,
                    CouplingType = CoachSearch.CouplingType,
                    Depot_Base = CoachSearch.BaseDepot,
                    ToiletSystem = CoachSearch.ToiletSystem,
                    AC_Flag = CoachSearch.AC_Flag
                });
            return p;
           }
      return null;
    }

    public IEnumerable<uspcCoachSearch_Result> GetCoachSearch(CoachSearchModel model)
    {
      string coachNo = string.Empty;
      string workshopid = string.Empty;
      string code = string.Empty;
      string datein = string.Empty;
      string mfgtype = string.Empty;
      string division = string.Empty;
      if (!string.IsNullOrEmpty(model.coachid))
        coachNo = model.coachid;
      if (!string.IsNullOrEmpty(model.Workshopid))
        workshopid = model.Workshopid;
      if (!string.IsNullOrEmpty(model.code))
        code = model.code;
      if (!string.IsNullOrEmpty(model.datein))
        datein = model.datein;
      if (!string.IsNullOrEmpty(model.mfgtype))
        mfgtype = model.mfgtype;
      if (!string.IsNullOrEmpty(model.division))
        division = model.division;
            var searchResult = context.uspcCoachSearch(coachNo, workshopid, code, datein, mfgtype, division).ToList();
            return searchResult;
    }
  }
}
