﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.WorkshopConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class WorkshopConnector : IWorkshop<WorkshopDTO>
  {
    private CoachMaster_V_2018Entities context;

    public WorkshopConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<WorkshopDTO> Getall()
    {
      IEnumerable<getworkshop_Result> list = (IEnumerable<getworkshop_Result>) this.context.getworkshop().ToList<getworkshop_Result>();
      if (list.Any())
        {
            IEnumerable<WorkshopDTO> p = list.Select(Workshop => new WorkshopDTO()
            {
                ID = Workshop.ID,
                Name = Workshop.Name
            });
            return p;
        }
            return null;
    }
  }
}
