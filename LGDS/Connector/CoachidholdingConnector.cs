﻿
using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class CoachidholdingConnector : ICoachDetails<CoachidholdingDTO>
  {
    private CoachMaster_V_2018Entities context;

    public CoachidholdingConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<CoachidholdingDTO> Getbyid(CoachSearchModel model)
    {
         IEnumerable<coachidholding_Result> coachSearch = GetCoachSearch(model);
        if (coachSearch.Any())
        { 
            IEnumerable<CoachidholdingDTO> p = coachSearch.Select(Coachidholding => new CoachidholdingDTO
            {
                Id = Coachidholding.Id,
                Name = Coachidholding.Name
            });
            return p;
        }
      return null;
    }

    public IEnumerable<coachidholding_Result> GetCoachSearch(CoachSearchModel model)
    {
      string coachNo = string.Empty;
      string workshopid = string.Empty;
      string code = string.Empty;
      string datein = string.Empty;
      string mfgtype = string.Empty;
      string division = string.Empty;
      if (!string.IsNullOrEmpty(model.coachid))
        coachNo = model.coachid;
      if (!string.IsNullOrEmpty(model.Workshopid))
        workshopid = model.Workshopid;
      if (!string.IsNullOrEmpty(model.code))
        code = model.code;
      if (!string.IsNullOrEmpty(model.datein))
        datein = model.datein;
      if (!string.IsNullOrEmpty(model.mfgtype))
        mfgtype = model.mfgtype;
      if (!string.IsNullOrEmpty(model.division))
        division = model.division;

      var searchResult =  context.coachidholding(coachNo, workshopid, code, datein, mfgtype, division).ToList();

            return searchResult;
        }
  }
}
