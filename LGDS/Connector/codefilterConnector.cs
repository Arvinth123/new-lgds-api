﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.codefilterConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class codefilterConnector : ICodefilter<codefilterDTO>
  {
    private CoachMaster_V_2018Entities context;

    public codefilterConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<codefilterDTO> Getbyid(codefilterModel model)
    {
      IEnumerable<uspcCodefilter_Result> source = Getcodefilter(model);
      if (source.Any())
        {
            IEnumerable<codefilterDTO> p = source.Select(codefilter => new codefilterDTO()
            {
                rows = codefilter.rows,
                code = codefilter.code,
                count = codefilter.count
            });
            return p;
        }
        return null;
    }

    public IEnumerable<uspcCodefilter_Result> Getcodefilter(codefilterModel model)
    {
      string empty = string.Empty;
      string workshopid = string.Empty;
      string code = string.Empty;
      string datein = string.Empty;
      string mfgtype = string.Empty;
      string division = string.Empty;
      if (!string.IsNullOrEmpty(model.Workshopid))
        workshopid = model.Workshopid;
      if (!string.IsNullOrEmpty(model.code))
        code = model.code;
      if (!string.IsNullOrEmpty(model.datein))
        datein = model.datein;
      if (!string.IsNullOrEmpty(model.mfgtype))
        mfgtype = model.mfgtype;
      if (!string.IsNullOrEmpty(model.division))
        division = model.division;
            var searchResult = context.uspcCodefilter(workshopid, code, datein, mfgtype, division).ToList();
            return searchResult;
    }
  }
}
