﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.MonthsConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class MonthsConnector : IMonths<MonthDTO>
  {
    private CoachMaster_V_2018Entities context;

    public MonthsConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<MonthDTO> Getbyid(CoachPositionModel model)
    {
      IEnumerable<uspcmonth_Result> months = GetMonths(model);
      if (months.Any())
        { 
            IEnumerable<MonthDTO> p = months.Select(Months => new MonthDTO()
            {
                name = Months.name,
                outturn = Months.outturn,
                ac = Months.ac,
                nac = Months.nac,
                spv = Months.spv,
                lhb = Months.lhb
            });
            return p;
        }
            return null;
    }

    public IEnumerable<uspcmonth_Result> GetMonths(CoachPositionModel model)
    {
      string month = string.Empty;
      string year = string.Empty;
      if (!string.IsNullOrEmpty(model.Month))
        month = model.Month;
      if (!string.IsNullOrEmpty(model.Year))
        year = model.Year;
            var searchResult = context.uspcmonth(month, year).ToList();
            return searchResult;
    }
  }
}
