﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.CoachidinturnConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class CoachidinturnConnector : IInTurn<CoachidinturnDTO>
  {
    private CoachMaster_V_2018Entities context;

    public CoachidinturnConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<CoachidinturnDTO> Getbyid(InTurnModel model)
    {
      IEnumerable<coachidinTurn_Result> coachSearch = GetCoachSearch(model);
      if (coachSearch.Any())
            { 
                IEnumerable<CoachidinturnDTO> p = coachSearch.Select(CoachidinturnSearch => new CoachidinturnDTO()
                {
                  Id = CoachidinturnSearch.Id,
                  Name = CoachidinturnSearch.Name
                });
                return p;
            }
         return null;
    }

    public IEnumerable<coachidinTurn_Result> GetCoachSearch(InTurnModel model)
    {
      string workin = string.Empty;
      string workout = string.Empty;
      string workshop = string.Empty;
      string coachNo = string.Empty;
      if (!string.IsNullOrEmpty(model.Workin))
        workin = model.Workin;
      if (!string.IsNullOrEmpty(model.Workout))
        workout = model.Workout;
      if (!string.IsNullOrEmpty(model.Workshop))
        workshop = model.Workshop;
      if (!string.IsNullOrEmpty(model.coachid))
        coachNo = model.coachid;

            var searchResult = context.coachidinTurn(workin, workout, workshop, coachNo).ToList();

            return searchResult;
    }
  }
}
