﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.CoachMasterConnecters
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using Moq.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class CoachMasterConnecters : IConnector<CoachMasterDTO>
  {
    private CoachMaster_V_2018Entities context;
    private IMapper<tbl_CoachMaster, CoachMasterDTO> CoachMasterMapper;
    private IMapper<tbl_TransactionalData, TransactionalDataDTO> POHDetailsMapper;

    public CoachMasterConnecters(CoachMaster_V_2018Entities _context, IMapper<tbl_CoachMaster, CoachMasterDTO> _CoachMasterMapper, IMapper<tbl_TransactionalData, TransactionalDataDTO> _POHDetailsMapper)
    {
      this.context = _context;
      this.CoachMasterMapper = _CoachMasterMapper;
      this.POHDetailsMapper = _POHDetailsMapper;
    }

    public void Create(CoachMasterDTO obj)
    {
      throw new NotImplementedException();
    }

    public void Delete(int? id)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<CoachMasterDTO> GetAll()
    {
      return this.context.tbl_CoachMaster.AsEnumerable<tbl_CoachMaster>().Select<tbl_CoachMaster, CoachMasterDTO>((Func<tbl_CoachMaster, CoachMasterDTO>) (bm => this.CoachMasterMapper.Map(bm, (CoachMasterDTO) null)));
    }

    public IEnumerable<CoachMasterDTO> GetById(string id)
    {
      return this.context.tbl_CoachMaster.AsEnumerable<tbl_CoachMaster>().Select<tbl_CoachMaster, CoachMasterDTO>((Func<tbl_CoachMaster, CoachMasterDTO>) (cm => this.CoachMasterMapper.Map(cm, (CoachMasterDTO) null))).Where<CoachMasterDTO>((Func<CoachMasterDTO, bool>) (cm => cm.CoachNo == id));
    }

    public void Update(CoachMasterDTO obj)
    {
      throw new NotImplementedException();
    }
  }
}
