﻿using LGDS.Interfaces;
using LGDS.Models;
using Moq.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace LGDS.Connectors
{
    public class ReportPDFConnector : IReportPDF<ReportPDFDTO>
    {
        private CoachMaster_V_2018Entities context;

        public ReportPDFConnector(CoachMaster_V_2018Entities _context)
        {
            this.context = _context;
        }

        public IList<string> Getbyid(ReportPDFModel model)
        {
            var date = "0" + model.Day;
            var month = "0" + model.Month;
            IQueryable<ReportPDF> Report;
            if (model.Day == "All")
            {
                Report = from r in context.ReportPDFs
                         where (r.Month == model.Month || r.Month == month) && r.Year == model.Year
                         select r;
            }
            else
            {

                Report = from r in context.ReportPDFs
                         where (r.Day == model.Day || r.Day == date) && (r.Month == model.Month || r.Month == month) && r.Year == model.Year
                         select r;
            }
            var Rep = Report.Select(x => x.Udate).Distinct();
            var Reports = new List<string>();
            foreach (var r in Rep)
            {
                Reports.Add(r.ToString("dd-MM-yyyy", (IFormatProvider)CultureInfo.InvariantCulture));
            }
            return Reports;
        }

        public IEnumerable<ReportPDFDTO> Getbydate(DateTime date)
        {
            IEnumerable<ReportPDF> Report;
            var mode = Convert.ToDateTime(date.ToString("yyyy-MM-dd", (IFormatProvider)CultureInfo.InvariantCulture));
            Report = from r in context.ReportPDFs
                     where r.Udate == mode
                     select r;
            IEnumerable<ReportPDFDTO> p = Report.Select(Reportview =>
            {
                ReportPDFDTO ReportPDFdto = new ReportPDFDTO();
                ReportPDFdto.Id = Reportview.Id;
                ReportPDFdto.Date = Reportview.Udate.ToString("dd-MM-yyyy", (IFormatProvider)CultureInfo.InvariantCulture);
                ReportPDFdto.Day = Reportview.Day;
                ReportPDFdto.Month = Reportview.Month;
                ReportPDFdto.Year = Reportview.Year;
                ReportPDFdto.pdf = Reportview.pdf;
                return ReportPDFdto;
            });

            return p;
        }
    }
}