﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.POHDetailsConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using Moq.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class POHDetailsConnector : IConnector<TransactionalDataDTO>
  {
    private CoachMaster_V_2018Entities context;
    private IMapper<tbl_TransactionalData, TransactionalDataDTO> POHDetailsMapper;
    private IMapper<tbl_CoachMaster, CoachMasterDTO> CoachMasterMapper;

    public POHDetailsConnector(CoachMaster_V_2018Entities _context, IMapper<tbl_TransactionalData, TransactionalDataDTO> _POHDetailsMapper, IMapper<tbl_CoachMaster, CoachMasterDTO> _CoachMasterMapper)
    {
      this.context = _context;
      this.POHDetailsMapper = _POHDetailsMapper;
      this.CoachMasterMapper = _CoachMasterMapper;
    }

    public void Create(TransactionalDataDTO obj)
    {
      throw new NotImplementedException();
    }

    public void Delete(int? id)
    {
      throw new NotImplementedException();
    }

    public IEnumerable<TransactionalDataDTO> GetAll()
    {
      return this.context.tbl_TransactionalData.AsEnumerable<tbl_TransactionalData>().Select<tbl_TransactionalData, TransactionalDataDTO>((Func<tbl_TransactionalData, TransactionalDataDTO>) (bm => this.POHDetailsMapper.Map(bm, (TransactionalDataDTO) null)));
    }

    public IEnumerable<TransactionalDataDTO> GetById(string id)
    {
      return this.context.tbl_TransactionalData.AsEnumerable<tbl_TransactionalData>().Select<tbl_TransactionalData, TransactionalDataDTO>((Func<tbl_TransactionalData, TransactionalDataDTO>) (vt => this.POHDetailsMapper.Map(vt, (TransactionalDataDTO) null))).Where<TransactionalDataDTO>((Func<TransactionalDataDTO, bool>) (vt => vt.CoachNo == id));
    }

    public void Update(TransactionalDataDTO obj)
    {
      throw new NotImplementedException();
    }
  }
}
