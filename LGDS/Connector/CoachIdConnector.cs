﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.CoachIdConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class CoachIdConnector : ICoachId<CoachIdDTO>
  {
    private CoachMaster_V_2018Entities context;

    public CoachIdConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<CoachIdDTO> Getbyid(string id)
    {
      IEnumerable<getcoachno_Result> list = (IEnumerable<getcoachno_Result>) this.context.getcoachno(id).ToList<getcoachno_Result>();
      if (list.Any())
            {
                IEnumerable<CoachIdDTO> p = list.Select(CoachId => new CoachIdDTO()
        {
          ID = CoachId.ID,
          Name = CoachId.Name
        });
                return p;
            }
            return null;
    }
  }
}
