﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.OutTurnConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LGDS.Connectors
{
  public class OutTurnConnector : IOutTurn<OutTurnDTO>
  {
    private CoachMaster_V_2018Entities context;

    public OutTurnConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<OutTurnDTO> Getbyid(OutTurnModel model)
    {
      IEnumerable<uspcoutTurn_Result> coachSearch = GetCoachSearch(model);
        if (coachSearch.Any())
        {
            IEnumerable<OutTurnDTO> p = coachSearch.Select(OutTurnSearch =>
            {
                OutTurnDTO outTurnDto = new OutTurnDTO();
                outTurnDto.CoachNo = OutTurnSearch.CoachNo;
                outTurnDto.serial_number = OutTurnSearch.serial_number;
                outTurnDto.Trp_Code = OutTurnSearch.Code;
                outTurnDto.Owning_Railway = OutTurnSearch.Railway;
                outTurnDto.Mfg_Type = OutTurnSearch.Type;
                outTurnDto.Built = OutTurnSearch.Built;
                outTurnDto.Depot_Base = OutTurnSearch.BaseDepot;
                outTurnDto.LPOHDate = string.Format("{0:dd-MM-yyyy}", OutTurnSearch.LastPOH);
                outTurnDto.PRetDate = string.Format("{0:dd-MM-yyyy}", OutTurnSearch.PreviousReturnDate);
                outTurnDto.DateIN = OutTurnSearch.IN_Date.ToString("dd-MM-yyyy");
                outTurnDto.DateOUT = string.Format("{0:dd-MM-yyyy}", OutTurnSearch.Date_OUT);
                outTurnDto.RetDate = string.Format("{0:dd-MM-yyyy}", OutTurnSearch.Return_Date);
                outTurnDto.RepType = OutTurnSearch.RepairType;
                outTurnDto.RakeForm = OutTurnSearch.Rake_Formation;
                return outTurnDto;
            });
            return p;
        }
      return null;
    }

    public IEnumerable<uspcoutTurn_Result> GetCoachSearch(OutTurnModel model)
    {
      string workin = string.Empty;
      string workout = string.Empty;
      string workshop = string.Empty;
      string coachNo = string.Empty;
      if (!string.IsNullOrEmpty(model.Workin))
        workin = model.Workin;
      if (!string.IsNullOrEmpty(model.Workout))
        workout = model.Workout;
      if (!string.IsNullOrEmpty(model.Workshop))
        workshop = model.Workshop;
      if (!string.IsNullOrEmpty(model.coachid))
        coachNo = model.coachid;
            var searchResult = context.uspcoutTurn(workin, workout, workshop, coachNo).ToList();
            return searchResult;
    }
  }
}
