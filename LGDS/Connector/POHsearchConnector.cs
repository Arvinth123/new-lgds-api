﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.POHsearchConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LGDS.Connectors
{
  public class POHsearchConnector : IPOHDetails<POHsearchDTO>
  {
    private CoachMaster_V_2018Entities context;

    public POHsearchConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<POHsearchDTO> Getbyid(POHsearchModels model)
    {
      IEnumerable<uspcPOHsearch_Result> coachSearch = GetCoachSearch(model);
      if (coachSearch.Any())
        { 
            IEnumerable<POHsearchDTO> p = coachSearch.Select(POHSearch =>
            {
                POHsearchDTO poHsearchDto = new POHsearchDTO();
                poHsearchDto.CoachNo = POHSearch.CoachNo;
                poHsearchDto.LPOHDate = string.Format("{0:dd-MM-yyyy}", POHSearch.LastPOH);
                poHsearchDto.PRetDate = string.Format("{0:dd-MM-yyyy}", POHSearch.PreviousReturnDate);
                poHsearchDto.YardIN = string.Format("{0:dd-MM-yyyy}", POHSearch.YardIN_Date);
                poHsearchDto.DateIN = POHSearch.IN_Date.ToString("dd-MM-yyyy");
                poHsearchDto.DateOUT = string.Format("{0:dd-MM-yyyy}", POHSearch.Date_OUT);
                poHsearchDto.POHShop = POHSearch.POH_Shop;
                poHsearchDto.RepType = POHSearch.RepairType;
                poHsearchDto.CorrDecl = POHSearch.Corr_Decleration;
                poHsearchDto.CorrHrs = POHSearch.CORR_HRS;
                poHsearchDto.RetDate = string.Format("{0:dd-MM-yyyy}", POHSearch.Return_Date);
                poHsearchDto.RakeForm = POHSearch.Rake_Formation;
                poHsearchDto.IOHDate = string.Format("{0:dd-MM-yyyy}", POHSearch.IOH_Date);
                poHsearchDto.IOHStat = POHSearch.IOH_Station;
                poHsearchDto.ExtRetDate = string.Format("{0:dd-MM-yyyy}", POHSearch.Ext_Return_Date);
                return poHsearchDto;
            });
            return p;
        }
            return null;
    }

    public IEnumerable<uspcPOHsearch_Result> GetCoachSearch(POHsearchModels model)
    {
      string coachNo = string.Empty;
      string own_rlw = string.Empty;
      string trp_Code = string.Empty;
      string mfg_Type = string.Empty;
      string lPOHDate = string.Empty;
      string pRetDate = string.Empty;
      if (!string.IsNullOrEmpty(model.CoachNo))
        coachNo = model.CoachNo;
      if (!string.IsNullOrEmpty(model.own_rlw))
        own_rlw = model.own_rlw;
      if (!string.IsNullOrEmpty(model.Trp_Code))
        trp_Code = model.Trp_Code;
      if (!string.IsNullOrEmpty(model.Mfg_Type))
        mfg_Type = model.Mfg_Type;
      if (!string.IsNullOrEmpty(model.LPOHDate))
        lPOHDate = model.LPOHDate;
      if (!string.IsNullOrEmpty(model.PRetDate))
        pRetDate = model.PRetDate;
            var searchResult = context.uspcPOHsearch(coachNo, own_rlw, trp_Code, mfg_Type, lPOHDate, pRetDate).ToList();
            return searchResult;
    }
  }
}
