﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.YearConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class YearConnector : IYear<YearDTO>
  {
    private CoachMaster_V_2018Entities context;

    public YearConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<YearDTO> Getbyid(CoachPositionModel model)
    {
      IEnumerable<uspcyear_Result> year = GetYear(model);
      if (year.Any())
        {
            IEnumerable<YearDTO> p = year.Select(Year => new YearDTO()
            {
                name = Year.name,
                outturn = Year.outturn,
                ac = Year.ac,
                nac = Year.nac,
                spv = Year.spv,
                lhb = Year.lhb
            });
            return p;
        }
            return null;
    }

    public IEnumerable<uspcyear_Result> GetYear(CoachPositionModel model)
    {
      string month = string.Empty;
      string year = string.Empty;
      if (!string.IsNullOrEmpty(model.Month))
        month = model.Month;
      if (!string.IsNullOrEmpty(model.Year))
        year = model.Year;
            var searchResult = context.uspcyear(month, year).ToList();
            return searchResult;
    }
  }
}
