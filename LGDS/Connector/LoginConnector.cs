﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.LoginConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class LoginConnector : ILogin<LoginDTO>
  {
    private CoachMaster_V_2018Entities context;

    public LoginConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<LoginDTO> Getbyid(LoginModel model)
    {
      IEnumerable<Nullable<int>> source = Getcodefilter(model);
      if (source.Any())
        { 
            IEnumerable<LoginDTO> p = source.Select(Login => new LoginDTO()
            {
                status = Login
            });
            return p;
        }
            return null;
    }

    public IEnumerable<Nullable<int>> Getcodefilter(LoginModel model)
    {
      string username = string.Empty;
      string password = string.Empty;
      string unit = string.Empty;
      if (!string.IsNullOrEmpty(model.username))
        username = model.username;
      if (!string.IsNullOrEmpty(model.password))
        password = model.password;
      if (!string.IsNullOrEmpty(model.unit))
        unit = model.unit;
            var searchResult = context.uspclogin(username, password, unit).ToList();
            return searchResult;
    }
  }
}
