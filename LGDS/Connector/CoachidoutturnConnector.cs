﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.CoachidoutturnConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class CoachidoutturnConnector : IOutTurn<CoachidoutturnDTO>
  {
    private CoachMaster_V_2018Entities context;

    public CoachidoutturnConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<CoachidoutturnDTO> Getbyid(OutTurnModel model)
    {
      IEnumerable<coachidoutTurn_Result> coachSearch = GetCoachSearch(model);
      if (coachSearch.Any())
        { 
            IEnumerable<CoachidoutturnDTO> p = coachSearch.Select(Coachidoutturn => new CoachidoutturnDTO()
            {
                Id = Coachidoutturn.Id,
                Name = Coachidoutturn.Name
            });
            return p;
        }
        return null;
    }

    public IEnumerable<coachidoutTurn_Result> GetCoachSearch(OutTurnModel model)
    {
      string workin = string.Empty;
      string workout = string.Empty;
      string workshop = string.Empty;
      string coachNo = string.Empty;
      if (!string.IsNullOrEmpty(model.Workin))
        workin = model.Workin;
      if (!string.IsNullOrEmpty(model.Workout))
        workout = model.Workout;
      if (!string.IsNullOrEmpty(model.Workshop))
        workshop = model.Workshop;
      if (!string.IsNullOrEmpty(model.coachid))
        coachNo = model.coachid;
            var searchResult = context.coachidoutTurn(workin, workout, workshop, coachNo).ToList();
            return searchResult;
    }
  }
}
