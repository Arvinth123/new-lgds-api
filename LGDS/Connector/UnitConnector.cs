﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.UnitConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class UnitConnector : IUnit<UnitDTO>
  {
    private CoachMaster_V_2018Entities context;

    public UnitConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

        public IEnumerable<UnitDTO> Getbyid()
        {
            IEnumerable<uspcunit_Result> list = (IEnumerable<uspcunit_Result>)this.context.uspcunit().ToList<uspcunit_Result>();
            if (list.Any<uspcunit_Result>())
            {
                IEnumerable<UnitDTO> p = list.Select(Unit => new UnitDTO()
                {
                    Id = Unit.Id,
                    Name = Unit.Name
                });
            return p;
           }
      return null;
    }
  }
}
