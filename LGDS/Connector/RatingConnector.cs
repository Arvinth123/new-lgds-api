﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.RatingConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class RatingConnector : IRating<RatingDTO>
  {
    private CoachMaster_V_2018Entities context;

    public RatingConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<RatingDTO> Getbyid(RatingModel model)
    {
      IEnumerable<string> coachSearch = GetCoachSearch(model);
      if (coachSearch.Any())
        {
            IEnumerable<RatingDTO> p = coachSearch.Select(CoachSearch => new RatingDTO()
            {
                status = CoachSearch
            });
            return p;
        }
            return null;
    }

    public IEnumerable<string> GetCoachSearch(RatingModel model)
    {
      string username = string.Empty;
      DateTime? date = new DateTime?();
      int? review = new int?();
      string rating = string.Empty;
      if (!string.IsNullOrEmpty(model.username))
        username = model.username;
      if (model.date.HasValue)
        date = model.date;
      if (model.rating.HasValue)
        review = model.rating;
      if (!string.IsNullOrEmpty(model.review))
        rating = model.review;
            var searchResult = context.uspcupdaterating(username, date, review, rating).ToList();
            return searchResult;
    }
  }
}
