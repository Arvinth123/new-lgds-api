﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.coachidshopConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class coachidshopConnector : Icoachidshop<coachidshopDTO>
  {
    private CoachMaster_V_2018Entities context;

    public coachidshopConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<coachidshopDTO> Getbyid(coachidshopModel model)
    {
      IEnumerable<getcoachidshop_Result> coachidshop1 = GetCoachidshop(model);
      if (coachidshop1.Any())
        { 
            IEnumerable<coachidshopDTO> p = coachidshop1.Select(coachidshop => new coachidshopDTO()
            {
                ID = coachidshop.ID,
                Name = coachidshop.Name
            });
            return p;
        }
            return null;
    }

    public IEnumerable<getcoachidshop_Result> GetCoachidshop(coachidshopModel model)
    {
      string coach = string.Empty;
      string shop = string.Empty;
      if (!string.IsNullOrEmpty(model.coachid))
        coach = model.coachid;
      if (!string.IsNullOrEmpty(model.shop))
        shop = model.shop;
            var searchResult = context.getcoachidshop(coach, shop).ToList();
            return searchResult;
    }
  }
}
