﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.InTurnConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LGDS.Connectors
{
  public class InTurnConnector : IInTurn<InTurnDTO>
  {
    private CoachMaster_V_2018Entities context;

    public InTurnConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<InTurnDTO> Getbyid(InTurnModel model)
    {
      IEnumerable<uspcinTurn_Result> coachSearch = GetCoachSearch(model);
      if (coachSearch.Any())
        { 
            IEnumerable<InTurnDTO>p = coachSearch.Select(InTurnSearch =>
            {
                InTurnDTO inTurnDto = new InTurnDTO();
                inTurnDto.CoachNo = InTurnSearch.CoachNo;
                inTurnDto.serial_number = InTurnSearch.serial_number;
                inTurnDto.Trp_Code = InTurnSearch.Code;
                inTurnDto.Owning_Railway = InTurnSearch.Railway;
                inTurnDto.Mfg_Type = InTurnSearch.Type;
                inTurnDto.Built = InTurnSearch.Built;
                inTurnDto.Depot_Base = InTurnSearch.BaseDepot;
                inTurnDto.LPOHDate = string.Format("{0:dd-MM-yyyy}", InTurnSearch.LastPOH);
                inTurnDto.PRetDate = string.Format("{0:dd-MM-yyyy}", InTurnSearch.PreviousReturnDate);
                inTurnDto.DateIN = InTurnSearch.IN_Date.ToString("dd-MM-yyyy");
                inTurnDto.DateOUT = string.Format("{0:dd-MM-yyyy}", InTurnSearch.Date_OUT);
                inTurnDto.RetDate = string.Format("{0:dd-MM-yyyy}", InTurnSearch.Return_Date);
                inTurnDto.RepType = InTurnSearch.RepairType;
                inTurnDto.RakeForm = InTurnSearch.Rake_Formation;
                return inTurnDto;
            });
            return p;
        }
            return null;
    }

    public IEnumerable<uspcinTurn_Result> GetCoachSearch(InTurnModel model)
    {
      string workin = string.Empty;
      string workout = string.Empty;
      string workshop = string.Empty;
      string coachNo = string.Empty;
      if (!string.IsNullOrEmpty(model.Workin))
        workin = model.Workin;
      if (!string.IsNullOrEmpty(model.Workout))
        workout = model.Workout;
      if (!string.IsNullOrEmpty(model.Workshop))
        workshop = model.Workshop;
      if (!string.IsNullOrEmpty(model.coachid))
        coachNo = model.coachid;
            var searchResult = context.uspcinTurn(workin, workout, workshop, coachNo).ToList();
            return searchResult;
    }
  }
}
