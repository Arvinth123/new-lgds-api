﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Connectors.WorkdaysConnector
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

using LGDS;
using LGDS.Interfaces;
using LGDS.Models;
using LGDSDTO.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LGDS.Connectors
{
  public class WorkdaysConnector : IWorkdays<WorkdaysDTO>
  {
    private CoachMaster_V_2018Entities context;

    public WorkdaysConnector(CoachMaster_V_2018Entities context)
    {
      this.context = context;
    }

    public IEnumerable<WorkdaysDTO> Getbyid(CoachPositionModel model)
    {
      IEnumerable<string> total = GetTotal(model);
        if (total.Any())
        {
            IEnumerable<WorkdaysDTO> p = total.Select(Workdays => new WorkdaysDTO()
            {
                WorkingDays = Workdays
            });
            return p;
        }
      return null;
    }

    public IEnumerable<string> GetTotal(CoachPositionModel model)
    {
      string month = string.Empty;
      string year = string.Empty;
      if (!string.IsNullOrEmpty(model.Month))
        month = model.Month;
      if (!string.IsNullOrEmpty(model.Year))
        year = model.Year;
            var searchResult = context.uspcworkdays(month, year).ToList();
            return searchResult;
    }
  }
}
