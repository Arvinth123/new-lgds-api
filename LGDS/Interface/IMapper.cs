﻿// Decompiled with JetBrains decompiler
// Type: LGDSapi.Interfaces.IMapper`2
// Assembly: LGDSapi, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 796B55D3-A184-4A9E-B381-976D86950290
// Assembly location: C:\LGDS\LGDS\bin\LGDSapi.dll

namespace LGDS.Interfaces
{
  public interface IMapper<fromObject, toObject>
  {
    toObject Map(fromObject fromObj, toObject toObj);
  }
}
