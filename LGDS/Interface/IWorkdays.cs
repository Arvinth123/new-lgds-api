using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IWorkdays<T>
	{
		IEnumerable<T> Getbyid(CoachPositionModel model);
	}
}
