using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IRating<T>
	{
		IEnumerable<T> Getbyid(RatingModel model);
	}
}
