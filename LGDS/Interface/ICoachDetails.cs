using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface ICoachDetails<T>
	{
		IEnumerable<T> Getbyid(CoachSearchModel model);
	}
}
