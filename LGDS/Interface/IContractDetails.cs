using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IContractDetails<T>
	{
		IEnumerable<T> GetById(string id);
	}
}
