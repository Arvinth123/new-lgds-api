using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IUnit<T>
	{
		IEnumerable<T> Getbyid();
	}
}
