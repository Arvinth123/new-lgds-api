using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IWorkshop<T>
	{
		IEnumerable<T> Getall();
	}
}
