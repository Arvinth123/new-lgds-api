using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IOutTurn<T>
	{
		IEnumerable<T> Getbyid(OutTurnModel model);
	}
}
