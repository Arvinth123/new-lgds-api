﻿using LGDS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LGDS.Interfaces
{
    public interface IReportPDF<T>
    {
        IList<string> Getbyid(ReportPDFModel model);

        IEnumerable<T> Getbydate(DateTime date);
    }
}