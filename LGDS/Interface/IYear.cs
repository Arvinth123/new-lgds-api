using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IYear<T>
	{
		IEnumerable<T> Getbyid(CoachPositionModel model);
	}
}
