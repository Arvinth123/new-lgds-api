using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface ILogin<T>
	{
		IEnumerable<T> Getbyid(LoginModel model);
	}
}
