using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IUserMaster<T>
	{
		IEnumerable<T> GetAll();

		T GetUserDetails(string userName);
	}
}
