using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface Icoachidshop<T>
	{
		IEnumerable<T> Getbyid(coachidshopModel model);
	}
}
