using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface ICodefilter<T>
	{
		IEnumerable<T> Getbyid(codefilterModel model);
	}
}
