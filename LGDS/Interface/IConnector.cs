using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IConnector<T>
	{
		IEnumerable<T> GetAll();

		IEnumerable<T> GetById(string id);

		void Create(T obj);

		void Update(T obj);

		void Delete(int? id);
	}
}
