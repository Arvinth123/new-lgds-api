using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface ICoachId<T>
	{
		IEnumerable<T> Getbyid(string id);
	}
}
