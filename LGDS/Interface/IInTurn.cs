using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IInTurn<T>
	{
		IEnumerable<T> Getbyid(InTurnModel model);
	}
}
