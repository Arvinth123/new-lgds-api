using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface ITotal<T>
	{
		IEnumerable<T> Getbyid(CoachPositionModel model);
	}
}
