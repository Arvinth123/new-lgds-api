using LGDS.Models;
using System;
using System.Collections.Generic;

namespace LGDS.Interfaces
{
	public interface IPOHDetails<T>
	{
		IEnumerable<T> Getbyid(POHsearchModels model);
	}
}
