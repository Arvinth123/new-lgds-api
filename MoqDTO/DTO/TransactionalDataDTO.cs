﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moq.DTO.DTO
{
    public class TransactionalDataDTO
    {
        public string CoachNo { get; set; }
        public DateTime? LastPOH { get; set; }
        public DateTime? PreviousReturnDate { get; set; }
        public DateTime? YardIN_Date { get; set; }
        public DateTime IN_Date { get; set; }
        public DateTime? NC_Date { get; set; }
        public DateTime? Date_OUT { get; set; }
        public string POH_Shop { get; set; }
        public string RepairType { get; set; }
        public string WorkOrder_No { get; set; }
        public string Rake_Formation { get; set; }
        public DateTime? Return_Date { get; set; }
        public DateTime? Ext_Return_Date { get; set; }
        public string IOH_Station { get; set; }
        public DateTime? IOH_Date { get; set; }
        public string CORR_HRS { get; set; }
        public string Corr_Decleration { get; set; }
        public string CYCLETIME_CATEGORY { get; set; }
        public string POH_Status { get; set; }
    }
}
