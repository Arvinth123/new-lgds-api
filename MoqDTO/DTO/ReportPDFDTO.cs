﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Moq.DTO.DTO
{
    public class ReportPDFDTO
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string pdf { get; set; }
    }
}
