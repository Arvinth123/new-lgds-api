using System;

namespace LGDSDTO.DTO
{
	public class CoachMasterDTO
	{
        public string CoachNo { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public string Railway { get; set; }
        public string Built { get; set; }
        public string VehicleType { get; set; }
        public string Category { get; set; }
        public string AC_Flag { get; set; }
        public string CoachID { get; set; }
        public string BaseDepot { get; set; }
        public string OwningDivision { get; set; }
        public string Base { get; set; }
        public string Workshop { get; set; }
        public DateTime? BuiltDate { get; set; }
        public DateTime? InductionDate { get; set; }
        public string CouplingType { get; set; }
        public string Coupling_Make { get; set; }
        public string BrakeSystem { get; set; }
        public string ToiletSystem { get; set; }
        public string Toilet_Make { get; set; }
        public string Periodicity { get; set; }
        public string Status { get; set; }
        public string Transfered_From { get; set; }
        public DateTime? Transfered_From_Date { get; set; }
        public string Transfered_To { get; set; }
        public DateTime? Transfered_To_Date { get; set; }
        public string Condemnation_No { get; set; }
        public DateTime? Condemnation_Date { get; set; }
        public string MLR_Shop { get; set; }
        public DateTime? MLR_Date { get; set; }
        public string RF_Shop { get; set; }
        public DateTime? RF_Date { get; set; }
        public string US_RM { get; set; }
        public string UpdationType { get; set; }
        public string NewCoachLrNo { get; set; }
        public DateTime? NewCoachLrDate { get; set; }
        public string No_of_Tanks { get; set; }
        public string OldCoachNo { get; set; }
        public string Remarks { get; set; }
        public DateTime? NextPOHDueOn { get; set; }
    }
}
