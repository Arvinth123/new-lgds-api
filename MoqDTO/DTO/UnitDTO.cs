using System;

namespace LGDSDTO.DTO
{
	public class UnitDTO
	{
		public string Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}
	}
}
