using System;

namespace LGDSDTO.DTO
{
	public class TotalDTO
	{
		public string name
		{
			get;
			set;
		}

		public string year
		{
			get;
			set;
		}

		public string target1
		{
			get;
			set;
		}

		public string actual1
		{
			get;
			set;
		}

		public string status1
		{
			get;
			set;
		}
	}
}
