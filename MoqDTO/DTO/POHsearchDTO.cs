using System;

namespace LGDSDTO.DTO
{
	public class POHsearchDTO
	{
		public string CoachNo
		{
			get;
			set;
		}

		public string LPOHDate
		{
			get;
			set;
		}

		public string PRetDate
		{
			get;
			set;
		}

		public string YardIN
		{
			get;
			set;
		}

		public string DateIN
		{
			get;
			set;
		}

		public string DateOUT
		{
			get;
			set;
		}

		public string POHShop
		{
			get;
			set;
		}

		public string RepType
		{
			get;
			set;
		}

		public string CorrDecl
		{
			get;
			set;
		}

		public string CorrHrs
		{
			get;
			set;
		}

		public string RetDate
		{
			get;
			set;
		}

		public string RakeForm
		{
			get;
			set;
		}

		public string IOHDate
		{
			get;
			set;
		}

		public string IOHStat
		{
			get;
			set;
		}

		public string ExtRetDate
		{
			get;
			set;
		}
	}
}
