using System;

namespace LGDSDTO.DTO
{
	public class WorkshopDTO
	{
		public string ID
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}
	}
}
