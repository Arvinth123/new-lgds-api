using System;

namespace LGDSDTO.DTO
{
	public class POHDetailsDTO
	{
		public string CoachNo
		{
			get;
			set;
		}

		public string LPOHDate
		{
			get;
			set;
		}

		public string PRetDate
		{
			get;
			set;
		}

		public DateTime? YardIN
		{
			get;
			set;
		}

		public DateTime DateIN
		{
			get;
			set;
		}

		public DateTime? DateOUT
		{
			get;
			set;
		}

		public string POHShop
		{
			get;
			set;
		}

		public string RepType
		{
			get;
			set;
		}

		public string CorrDecl
		{
			get;
			set;
		}

		public string CorrHrs
		{
			get;
			set;
		}

		public DateTime? RetDate
		{
			get;
			set;
		}

		public string RakeForm
		{
			get;
			set;
		}

		public string IOHDate
		{
			get;
			set;
		}

		public string IOHStat
		{
			get;
			set;
		}

		public string ExtRetDate
		{
			get;
			set;
		}

		public int? RCount
		{
			get;
			set;
		}

		public string yardin1
		{
			get;
			set;
		}

		public string datein1
		{
			get;
			set;
		}

		public string dateout1
		{
			get;
			set;
		}

		public string retdate1
		{
			get;
			set;
		}
	}
}
