using System;

namespace LGDSDTO.DTO
{
	public class CoachSearchDTO
	{
		public string CoachNo
		{
			get;
			set;
		}

		public long? SerailNo
		{
			get;
			set;
		}

		public string Code
		{
			get;
			set;
		}

		public string LPOHDate
		{
			get;
			set;
		}

		public string RetDate
		{
			get;
			set;
		}

		public string DateIN
		{
			get;
			set;
		}

		public string Mfg_Type
		{
			get;
			set;
		}

		public string Owning_Railway
		{
			get;
			set;
		}

		public string Division
		{
			get;
			set;
		}

		public string Built
		{
			get;
			set;
		}

		public string CouplingType
		{
			get;
			set;
		}

		public string Depot_Base
		{
			get;
			set;
		}

		public string ToiletSystem
		{
			get;
			set;
		}

		public string AC_Flag
		{
			get;
			set;
		}
	}
}
