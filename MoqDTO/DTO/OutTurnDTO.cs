using System;

namespace LGDSDTO.DTO
{
	public class OutTurnDTO
	{
		public string CoachNo
		{
			get;
			set;
		}

		public long? serial_number
		{
			get;
			set;
		}

		public string Trp_Code
		{
			get;
			set;
		}

		public string Owning_Railway
		{
			get;
			set;
		}

		public string Mfg_Type
		{
			get;
			set;
		}

		public string Built
		{
			get;
			set;
		}

		public string Depot_Base
		{
			get;
			set;
		}

		public string LPOHDate
		{
			get;
			set;
		}

		public string PRetDate
		{
			get;
			set;
		}

		public string DateIN
		{
			get;
			set;
		}

		public string DateOUT
		{
			get;
			set;
		}

		public string RetDate
		{
			get;
			set;
		}

		public string RepType
		{
			get;
			set;
		}

		public string RakeForm
		{
			get;
			set;
		}
	}
}
