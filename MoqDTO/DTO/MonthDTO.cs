using System;

namespace LGDSDTO.DTO
{
	public class MonthDTO
	{
		public string name
		{
			get;
			set;
		}

		public string outturn
		{
			get;
			set;
		}

		public string ac
		{
			get;
			set;
		}

		public string nac
		{
			get;
			set;
		}

		public string spv
		{
			get;
			set;
		}

		public string lhb
		{
			get;
			set;
		}
	}
}
