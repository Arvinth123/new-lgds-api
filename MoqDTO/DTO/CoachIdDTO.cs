using System;

namespace LGDSDTO.DTO
{
	public class CoachIdDTO
	{
		public string ID
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}
	}
}
