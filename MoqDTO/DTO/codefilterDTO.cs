using System;

namespace LGDSDTO.DTO
{
	public class codefilterDTO
	{
		public int? rows
		{
			get;
			set;
		}

		public string code
		{
			get;
			set;
		}

		public int? count
		{
			get;
			set;
		}
	}
}
